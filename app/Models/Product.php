<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Product extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'product_categories';
    protected $fillable = [
        'title',
        'price'
    ];

    public function items()
    {
        return $this->hasMany(Category::class);
    }
}
