<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function store(Request $request)
    {
        $product = new Product();

        $product->title = $request->title;
        $product->price = $request->price;
        $product->category = [
            'name' => $request->input('category.name')
        ];

        $product->save();

        return response()->json([$product], 201);
    }
}
