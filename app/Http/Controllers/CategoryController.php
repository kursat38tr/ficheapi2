<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function show()
    {
        $category = Category::all();

        return response()->json([$category], 201);
    }

    public function store(Request $request)
    {
        $category = new Category();

        $category->name = $request->name;

        $category->save();

        return response()->json([$category], 201);
    }
}
